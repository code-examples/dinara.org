# [dinara.org](https://dinara.org) source codes

<br/>

### Run dinara.org on localhost

    # vi /etc/systemd/system/dinara.org.service

Insert code from dinara.org.service

    # systemctl enable dinara.org.service
    # systemctl start dinara.org.service
    # systemctl status dinara.org.service

http://localhost:4049
